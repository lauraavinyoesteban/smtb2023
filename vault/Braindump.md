
**What i did initially presented to James**
However, after that, I usually have this Summer Schools where I do some (actually real) science with high school students (batxillerat). A nice idea would be to implement, or recreate classical limb experiments on LimbNet. I genuinely think it is a nice idea for a three weeks project (and who knows, we could end up with a very nice review / LimbNet publicity) + a nice way to test out models we have already done in the lab in harder conditions.

# To do:

1. Previously to the summer school @Lau will do a grafting in-silico experiment as part her thesis. 
2. Therefore, she will be able to:
	1. Understand better the base code that is needed. 
	2. Be able to handle the UI so it is easier for the students how to do the insilico experiments. 
3. We have to decide cool experiments to do:
	1. We can work on a field manner (the mesh we have now)
	2. As a graphs - https://academic.oup.com/bioinformatics/article/30/24/3598/2422217 and a paper that is unpublish for now. 
4. Also, would be interresting if @Vera reads:
	1. https://www.embl.org/groups/sharpe/
	2. https://europepmc.org/article/MED/26174932
	3. https://europepmc.org/article/MED/21347315


# Papers
- https://pubmed.ncbi.nlm.nih.gov/7430928/
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1571056/
- https://www.frontiersin.org/articles/10.3389/fcell.2017.00014/full
- https://link.springer.com/chapter/10.1007/978-1-4684-9057-2_40
- https://www.sciencedirect.com/science/article/pii/S0925477398000902

